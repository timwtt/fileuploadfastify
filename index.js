const fastify = require('fastify')()
const fs = require('fs')
const util = require('util')
const path = require('path')
const { pipeline } = require('stream')
const pump = util.promisify(pipeline)

fastify.register(require('fastify-multipart'))

fastify.post('/file', async function (req, reply) {
  const data = await req.file()

  console.log(data.filename)

  await pump(data.file, fs.createWriteStream(data.filename))  //

  reply.send(data.filename)
})

fastify.listen(3000, err => {
  if (err) throw err
  console.log(`server listening on ${fastify.server.address().port}`)
})
